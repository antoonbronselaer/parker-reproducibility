import csv
import numpy as np

def load_gold_standard(filename):
    gs = {}
    with open(filename, encoding="utf-8") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        d = {}
        row_id = None
        for row in csv_reader:
            if (row_id is None) or (row_id == int(row['tid'])):
                d.update({row['attribute'].lower(): row['correct_val'].lower()})
                row_id = int(row['tid'])
            else:
                gs.update({row_id: d})
                row_id = int(row['tid'])
                d = {}
                d.update({row['attribute'].lower(): row['correct_val'].lower()})
        gs.update({row_id: d})
    return gs


def load_dataset(filename, sep=',', qt='"', fillna=''):
    dataset = {}
    with open(filename, encoding="utf-8") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=sep)
        row_id = 0
        for row in csv_reader:
            if '_tid_' in row.keys():
                del row['_tid_']
            new_row = {}
            for k, v in row.items():
                #print(row_id, k)
                if v == '' and fillna:
                    new_row.update({k.lower() : fillna})
                else:
                    new_row.update({k.lower() : v.lower()})
            #row = dict((k.lower(), v.lower()) for k, v in row.items())
            dataset.update({row_id: new_row})
            row_id = row_id + 1
    return dataset

def get_errors_per_att(gs, original, keys=[], verbose=False): 
    
    #errors_per_att = {}
    #for i in ((original.keys()[0].keys() & gs.keys()[0].keys()) - set(keys)):
    #    errors_per_att[i] = 0

    #print(errors_per_att)
    #return errors_per_att
        
    errors = 0
    for i in original.keys() & gs.keys():
        for k in gs[i].keys():
            if k not in keys:
                if gs[i][k] != original[i][k]:
                    errors = errors + 1
                    
    print(errors)
                    

def get_errors_metrics(gs, original, repaired, keys=[], verbose=False):
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for i in original.keys() & gs.keys():
        for k in gs[i].keys():
            if k not in keys:
                # TP: Incorrect cell, error detected
                if gs[i][k] != original[i][k] and original[i][k] != repaired[i][k]:
                    tp = tp + 1
                # FP: Correct cell, error detected
                elif gs[i][k] == original[i][k] and original[i][k] != repaired[i][k]:
                    fp = fp + 1
                # TN: Correct cell, no error detected
                elif gs[i][k] == original[i][k] and original[i][k] == repaired[i][k]:
                    tn = tn +1
                # FN: Incorrect cell, no error detected
                elif gs[i][k] != original[i][k] and original[i][k] == repaired[i][k]:
                    fn = fn + 1
                else:
                    print("Something went wrong with this cell")
                    print(i, k)
                    print("gs", gs[i][k], "o", original[i][k], "r", repaired[i][k])

    if verbose:
        print("Metrics for errors")
        print("TP", tp)
        print("FP", fp)
        print("TN", tn)
        print("FN", fn)
        print("Precision", tp / float(tp + fp))
        print("Recall", tp / float(tp + fn))
        print()
    precision = tp / float(tp + fp)
    recall = tp / float(tp + fn)
    return {'tp': tp, 'tn': tn, 'fp': fp, 'fn': fn, 'precision': precision, 'recall': recall}


def get_new_repairs_metrics(gs, original, repaired, keys=[], verbose=False, sampled_tuples=[]):
    tp = 0
    fp = 0
    output = 0
    e = []
    repairs_in_gs = 0

    sampled_keys = [gs[i][keys[0]] for i in sampled_tuples]

    print("sampled_keys", sampled_keys)

    print("tuples to analyse", len((original.keys() & gs.keys()) - set(sampled_tuples)))
    for i in (original.keys() & gs.keys()) - set(sampled_tuples):

        # Remove tuples with the same key from analysis
        # if gs[i][keys[0]] in sampled_keys:
        #    continue

        for k in gs[i].keys():
            if k not in keys:
                # This cell was repaired.
                if original[i][k] != repaired[i][k]:
                    output = output + 1

                # Repair in GS.
                if gs[i][k] != original[i][k]:
                    repairs_in_gs = repairs_in_gs + 1
                    e.append((i, k))

    if tp + fp != 0:
        precision = tp / float(tp + fp)
    else:
        precision = float('nan')
    recall = tp / float(repairs_in_gs)

    if verbose:
        print("Metrics for repairs")
        print("TP", tp)
        print("FP", fp)
        print("repairs_in_gs", repairs_in_gs)
        print("Precision", tp / precision)
        print("Recall", tp / recall)
        print()

    print(output)


def get_repairs_metrics(gs, original, repaired, keys=[], verbose=False, sampled_tuples=[]):
    tp = 0
    fp = 0
    repairs_in_gs = 0

    sampled_keys = [gs[i][keys[0]] for i in sampled_tuples]
    for i in (original.keys() & gs.keys())-set(sampled_tuples):

        # Remove tuples used during training from analysis
        if gs[i][keys[0]] in sampled_keys:
            continue

        for k in gs[i].keys():
            if k not in keys:
                    
                # This cell was repaired.
                if original[i][k] != repaired[i][k]:
                    
                    # TP: Incorrect cell, repaired correctly
                    if gs[i][k] != original[i][k] and gs[i][k] == repaired[i][k]:
                        tp = tp + 1
                    # FP: Correct cell, repaired incorrectly
                    elif gs[i][k] == original[i][k] and gs[i][k] != repaired[i][k] and repaired[i][k] != '_nan_' and not(repaired[i][k].startswith('_L')):
                        fp = fp + 1
                    # FP: Incorrect cell, repaired incorrectly
                    elif gs[i][k] != original[i][k] and gs[i][k] != repaired[i][k] and repaired[i][k] != '_nan_' and not(repaired[i][k].startswith('_L')):
                        fp = fp + 1

                # Repair in GS.
                if gs[i][k] != original[i][k]:
                    repairs_in_gs = repairs_in_gs + 1

    if tp + fp != 0:
        precision = tp / float(tp + fp)
    else: 
        precision = float('nan')
    recall = tp / float(repairs_in_gs)
    
    if verbose:
        print("Metrics for repairs")
        print("TP", tp)
        print("FP", fp)
        print("repairs_in_gs", repairs_in_gs)
        print("Precision", precision)
        print("Recall", recall)
        print()
    
    return {'tp': tp, 'fp': fp, 'relevant': repairs_in_gs, 'precision': precision, 'recall': recall}


def get_repairs_macro_metrics(gs, original, repaired, keys=[], verbose=False, sampled_tuples=[]):
    e = []
    sampled_keys = [gs[i][keys[0]] for i in sampled_tuples]

    metrics = {}
    aux = list(gs.keys())[0]
    for k in gs[aux].keys() - set(keys):
        metrics.update({k : {'tp': 0, 'fp': 0, 'repairs_in_gs': 0}})

    for i in (original.keys() & gs.keys()) - set(sampled_tuples):
        # Remove tuples used during training     from analysis
        if gs[i][keys[0]] in sampled_keys:
            continue

        for k in gs[i].keys():
            if k not in keys:
                # This cell was repaired.
                if original[i][k] != repaired[i][k]:
                    # TP: Incorrect cell, repaired correctly
                    if gs[i][k] != original[i][k]  and gs[i][k] == repaired[i][k]:
                        metrics[k]['tp'] = metrics[k]['tp'] + 1
                        #tp = tp + 1
                    # FP: Correct cell, repaired incorrectly
                    elif gs[i][k] == original[i][k] and gs[i][k] != repaired[i][k] and repaired[i][k] != '_nan_':
                        metrics[k]['fp'] = metrics[k]['fp'] + 1
                        #fp = fp + 1
                    # FP: Incorrect cell, repaired incorrectly
                    elif gs[i][k] != original[i][k] and gs[i][k] != repaired[i][k] and repaired[i][k] != '_nan_':
                        metrics[k]['fp'] = metrics[k]['fp'] + 1
                        #fp = fp + 1
                # Repair in GS.
                if gs[i][k] != original[i][k]:
                    metrics[k]['repairs_in_gs'] = metrics[k]['repairs_in_gs'] + 1
                    #repairs_in_gs = repairs_in_gs + 1
                    e.append((i,k))

    macro_precision = []
    macro_recall = []
    for k in metrics.keys():
        if (metrics[k]['tp'] + metrics[k]['fp'] == 0):
            metrics[k]['precision'] = float('nan')
        else:            
            metrics[k]['precision'] = metrics[k]['tp'] / float(metrics[k]['tp'] + metrics[k]['fp'])
            macro_precision.append(metrics[k]['precision'])
            
        if metrics[k]['repairs_in_gs'] == 0:
            metrics[k]['recall'] = float('nan')
        else:
            metrics[k]['recall'] = metrics[k]['tp'] / float(metrics[k]['repairs_in_gs'])
            macro_recall.append(metrics[k]['recall'])
    
    res = {}
    res['metrics'] = metrics
    res['macro_precision'] = sum(macro_precision) / float(len(macro_precision))
    res['macro_recall'] = sum(macro_recall) / float(len(macro_recall))
                    
    if verbose:
        print("Macro metrics for repairs")
        print("Macro precision", res['macro_precision'])
        print("Macro recall", res['macro_recall'])
        print("Raw macro precision", macro_precision)
        print("Raw macro recall", macro_recall)
        print()

    return res


def load_results(results, sep=","):
    res = []
    for filename in results:
        print("Loading", filename)
        conf = filename.split(".csv")[0].rsplit("/")[-1]
        data = load_dataset(filename, sep=sep)
        res.append((conf,data)) 
    return res


def load_baran_results(repairs, runs):
    baran_repairs = []
    for i in range(0, runs): 
        r = load_dataset(repairs+str(i)+'.csv')
        with open(repairs+str(i)+'_sample.csv', 'r') as sample:
            s = eval(sample.readlines()[0])        
        baran_repairs.append((r, s.keys())) 
    return baran_repairs
        

def compute_baran_metrics(gs, dataset, data_repairs, keys, runs): 
    precision = 0 
    recall = 0
    macro_precision = 0
    macro_recall = 0
    macro_aux = []
    
    # Compute average precision and recall
    for (repairs, sample) in data_repairs:
        res = get_repairs_metrics(gs, dataset, repairs, keys=keys, sampled_tuples=sample)
        macro_aux.append(get_repairs_macro_metrics(gs, dataset, repairs, keys=keys, sampled_tuples=sample))
        precision += res["precision"]/float(runs)
        recall += res["recall"]/float(runs)
        
    # Compute average macro-precision and macro-recall
    metrics = {k: {"precision": [], "recall": []} for k in macro_aux[0]["metrics"].keys()} 
    final_metrics = {k: {"precision": 0, "recall": 0} for k in macro_aux[0]["metrics"].keys()} 
    for exp in macro_aux: 
        macro_precision += exp["macro_precision"]/float(runs)
        macro_recall += exp["macro_recall"]/float(runs)
        for attr in macro_aux[0]["metrics"].keys(): 
            if not np.isnan(exp["metrics"][attr]["precision"]):
                metrics[attr]["precision"].append(exp["metrics"][attr]["precision"]) 
            if  not np.isnan(exp["metrics"][attr]["recall"]):
                metrics[attr]["recall"].append(exp["metrics"][attr]["recall"])
        
    for attr in macro_aux[0]["metrics"].keys():       
        if len(metrics[attr]["precision"]):
            final_metrics[attr]["precision"] = sum(metrics[attr]["precision"])/float(len(metrics[attr]["precision"]))
        else:
            final_metrics[attr]["precision"] = np.nan
        if len(metrics[attr]["recall"]):
            final_metrics[attr]["recall"] = sum(metrics[attr]["recall"])/float(len(metrics[attr]["recall"]))
        else:
            final_metrics[attr]["recall"] = np.nan


    baran_metrics = {'precision': precision, 'recall': recall}
    baran_macro_metrics = {"macro_precision": macro_precision, 
                          "macro_recall": macro_recall, 
                          "metrics": final_metrics}
    
    return baran_metrics, baran_macro_metrics


if __name__ == '__main__':
    prefix = '/Users/maribelacosta/Documents/PycharmProjects/EditRules/data/'
    gold_standard = load_gold_standard(prefix+'eudract/new_eudract_holoclean_cleaned.csv')
    original = load_dataset(prefix+'eudract/eudract_sample4000.csv')
    repaired = load_dataset(prefix+'eudract/eudract_repaired_4000.csv')


    get_repairs_metrics(gold_standard, original, repaired)
    get_errors_metrics(gold_standard, original, repaired)


