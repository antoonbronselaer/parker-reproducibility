import pandas as pd

def eudract_rules(df, dataset='', approach=''):
    d = {}
    # E1
    try:
        d[dataset + 'E1'] = len(df[(df.double_blind == 'yes') & (df.open == 'yes')])
    except:
        d[dataset + 'E1'] = float("nan")
        print("Not possible")

    # E2
    try:
        d[dataset + 'E2'] = len(df[(df.single_blind == 'yes') & (df.open == 'yes')])
    except:
        d[dataset + 'E2'] = float("nan")

    # E3
    try:
        d[dataset + 'E3'] = len(df[(df.double_blind == 'no') & (df.single_blind == 'no') & (df.open == 'no')])
    except:
        d[dataset + 'E3'] = float("nan")

    # E4
    try:
        d[dataset + 'E4'] = len(df[(df.controlled == 'no') & (df.placebo == 'yes')])
    except:
        d[dataset + 'E4'] = float("nan")

    # E5
    try:
        d[dataset + 'E5'] = len(df[(df.crossover == 'yes') & (df.parallel_group == 'yes')])
    except:
        d[dataset + 'E5'] = float("nan")

    # E6
    try:
        d[dataset + 'E6'] = len(df[(df.double_blind == 'yes') & (df.single_blind == 'yes')])
    except:
        d[dataset + 'E6'] = float("nan")

    # E7
    try:
        d[dataset + 'E7'] = len(df[(df.active_comparator == 'yes') & (df.controlled == 'no')])
    except:
        d[dataset + 'E7'] = float("nan")

    # E8
    try:
        d[dataset + 'E8'] = len(df[((df.arms == '1') | (df.arms == '0')) & (df.placebo == 'yes')])
    except:
        d[dataset + 'E8'] = float("nan")

    # E9
    try:
        d[dataset + 'E9'] = len(df[((df.arms == '1') | (df.arms == '0')) & (df.active_comparator == 'yes')])
    except:
        d[dataset + 'E9'] = float("nan")

    print("Violations computed", approach, dataset)
    return pd.DataFrame([d], index=[approach])


def flight_rules(df, dataset='', approach=''):
    # E1
    d = {}
    try:
        d[dataset + 'E1'] = len(df[(df.actual_departure >= df.actual_arrival) & (df.actual_arrival != 'null') & (
                    df.actual_departure != 'null') & (df.actual_arrival != '') & (
                    df.actual_departure != '') & (df.actual_arrival != '?') & (
                    df.actual_departure != '?')])
    except:
        d[dataset + 'E1'] = float('nan')

    # E2
    try:
        d[dataset + 'E2'] = len(df[(df.scheduled_departure >= df.scheduled_arrival) & (
                    df.scheduled_arrival != 'null') & (df.scheduled_departure != 'null') & (
                    df.scheduled_arrival != '') & (df.scheduled_departure != '') & (
                    df.scheduled_arrival != '?') & (df.scheduled_departure != '?')])
    except:
        d[dataset + 'E2'] = float('nan')

    print("Violations computed", approach, dataset)
    return pd.DataFrame([d], index=[approach])


def allergen_rules(df, dataset='', approach=''): 
    
    # E1
    d = {}
    try:
        d[dataset+'E1'] = len(df[df.nuts < df.brazil_nuts])
    except:
        d[dataset+'E1'] = float('nan')
        
    # E2
    try:
        d[dataset+'E2'] = len(df[df.nuts < df.macadamia_nuts])
    except:
        d[dataset+'E2'] = float('nan')
        
    # phi3
    try:
        d[dataset+'E3'] = len(df[df.nuts < df.hazelnut])
    except:
        d[dataset+'E3'] = float('nan')
        
    # phi4
    try:
        d[dataset+'E4'] = len(df[df.nuts < df.pistachio])
    except:
        d[dataset+'E4'] = float('nan')
        
    # phi5
    try:
        d[dataset+'E5'] = len(df[df.nuts < df.walnut])
    except:
        d[dataset+'E5'] = float('nan')
        
    print("Violations computed", approach, dataset)
    return pd.DataFrame([d], index=[approach])

