import pandas
import os
import raha
import random
import time


def run(dataset_dictionary, output_file, sample_file):
    # Raha
    print("Run Raha")
    t1 = time.time()
    app_1 = raha.Detection()
    app_1.LABELING_BUDGET = 20
    app_1.SAVE_RESULTS = True 
    app_1.VERBOSE = True

    t2 = time.time()
    d = app_1.initialize_dataset(dataset_dictionary)
    app_1.run_strategies(d)
    app_1.generate_features(d)
    app_1.build_clusters(d)

    print("Labeling")
    while len(d.labeled_tuples) < app_1.LABELING_BUDGET:
        app_1.sample_tuple(d)
        if d.has_ground_truth:
            app_1.label_with_ground_truth(d)

    print("Propagate and predict")
    app_1.propagate_labels(d)
    app_1.predict_labels(d)

    if app_1.SAVE_RESULTS:
        app_1.store_results(d)
    t3 = time.time()

    # Baran
    print("Run Baran")
    app_2 = raha.Correction()
    app_2.LABELING_BUDGET = 0
    app_2.SAVE_RESULTS = False
    app_2.VERBOSE = True

    print("Initialize models and datasets")
    app_2.initialize_models(d)
    app_2.initialize_dataset(d)

    print("Using labels")
    for si in d.labeled_tuples:
        d.sampled_tuple = si
        app_2.update_models(d)
        app_2.generate_features(d)
        app_2.predict_corrections(d)
    t4 = time.time()

    if app_2.SAVE_RESULTS:
        app_2.store_results(d)

    print("Computing repaired data")
    d.create_repaired_dataset(d.corrected_cells)
    d.repaired_dataframe.to_csv(output_file, index=False)

    with open(sample_file, 'w') as sample_out:
         sample_out.write(str(d.labeled_tuples))

    t5 = time.time()

    print("preparation time", t2-t1)
    print("raha time", t3-t2)
    print("baran time", t4-t3)
    print("finalising", t5-t4)


def exp_allergen(i, outdir=''):
    dataset_name = "allergen" + str(i)
    dataset_dictionary = {

        "name": dataset_name,
        "path": "../datasets/allergen_baran.csv",
        "clean_path": "../datasets/allergen_baran_clean.csv"
    }
    outputfile = outdir+"raha_baran_allergen"+str(i)+".csv"
    samplefile = outdir+"raha_baran_allergen"+str(i)+"_sample.csv"
    run(dataset_dictionary, outputfile, samplefile)


def exp_eudract(i, outdir=''):
    dataset_name = "eudract" + str(i)
    dataset_dictionary = {
        "name": dataset_name,
        "path": "../datasets/eudract_baran.csv",
        "clean_path": "../datasets/eudract_baran_clean.csv"
    }
    outputfile = outdir+"raha_baran_eudract"+str(i)+".csv"
    samplefile = outdir+"raha_baran_eudract"+str(i)+"_sample.csv"
    run(dataset_dictionary, outputfile, samplefile)


if __name__ == "__main__":
    # Configuration
    runs = 10        # Number of runs to repeat each experiment
    outdir = "./"   # Directory to write the repairs and sampled tuples

    # Run approach on allergen dataset
    for i in range(0,runs):
        random.seed(i+1)
        exp_allergen(i, outdir)

    # Run approach on eudract dataset
    for i in range(0,runs):
        random.seed(i+1)
        exp_eudract(i, outdir)
