## Experiments with parker

Parker has been experimentally evaluated on three datasets: allergen, flight and eudract.
In these experiments, we tested several basic options for the cost model and the repair selection.
Below, we provide some guidelines to compile the best set-up for each of the three datasets.
In each case, we always assume a *full* key repair, meaning that all attributes involved in sigma rules are always under a key constraint.

### Eudract

For the Eudract dataset, Parker works best in the scenario where **constant** cost functions are combined with a **multiplier** that accounts for the reliability of a row.
The best results are obtained if the amplifier parameter is set to `4`.
For the selection of the final repair, it is recommended to use the **FrequencyRepairSelection**.
Hereby, it is imperative that the clean data is projected over the attributes that must be repaired, as shown below.
This projection ensures that the FP-tree built to represent the clean data, is as small as possible.


```java
public static void main(String[] args) throws DataReadException, FileNotFoundException, RepairException, IOException, DataWriteException
{
    //Create a binder to the file with the data
    CSVBinder csvBinder = new CSVBinder(
        new CSVProperties(true, ",", null),
        new File("eudract.csv"));

    //Read the data
    Dataset data = new CSVDataReader(csvBinder).readData();

    //Read the rules
    SigmaRuleset rules = ConstraintIo.readSigmaRuleSet(new File("eudract.rules"));
            
    //Build a sufficient set with the FCF generator
    SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset.create(rules, new FCFGenerator());

    //Build the partial key
    PartialKey partialkey = new PartialKey
    (
        Stream.of("eudract_number").collect(Collectors.toSet()),
        Stream.of(
            "single_blind",
            "double_blind",
            "open",
            "controlled",
            "placebo",
            "active_comparator",
            "randomised",
            "crossover",
            "parallel_group",
            "arms"
        ).collect(Collectors.toSet())
    );

    //All cost functions are constant
    Map<String, CostFunction<?>> costFunctions = new HashMap<>();
        
    costFunctions.put("single_blind", new ConstantCostFunction<>());
    costFunctions.put("double_blind", new ConstantCostFunction<>());
    costFunctions.put("open", new ConstantCostFunction<>());
    costFunctions.put("controlled", new ConstantCostFunction<>());
    costFunctions.put("placebo", new ConstantCostFunction<>());
    costFunctions.put("active_comparator", new ConstantCostFunction<>());
    costFunctions.put("randomised", new ConstantCostFunction<>());
    costFunctions.put("crossover", new ConstantCostFunction<>());
    costFunctions.put("parallel_group", new ConstantCostFunction<>());
    costFunctions.put("arms", new ConstantCostFunction<>());
            
    ParkerModel costModel = new ParkerModel(
        partialkey,
        costFunctions,
        new ErrorBasedObjectCost(rules, 4),
        sufficientRules
    );

    //Collect the clean data, but remove attributes that do not need repairs (attributes about provenance)
    Dataset cleanData = data
        .select(o -> sufficientRules.isSatisfied(o))
        .inverseProject(
            "eudract_number",
            "protocol_country_code",
            "source"
        );

    //Define a repair selector
    RepairSelection selector = new FrequencyRepairSelection(cleanData, sufficientRules);

    //Pass the cost model and the repair selector to make a parker engine
    ParkerRepair repairEngine = new ParkerRepair(
        costModel,
        selector
    );

    //Repair
    Dataset repaired = repairEngine.repair(data);

    //Write the repair to a file
    new CSVDataWriter(
        new CSVBinder(
            new CSVProperties(true, ",", null),
            new File("eudract_repair.csv"))
    ).writeData(repaired);
}
```

### Allergen

For the Allergen dataset, Parker works best in the scenario where **preference-based** cost functions are combined with a multiplier that accounts for the **reliability** of a row.
The best results are obtained if the amplifier parameter is set to `4`.
For the selection of the final repair, it is recommended to use the **FrequencyRepairSelection**.
Hereby, it is imperative that the clean data is projected over the attributes that must be repaired, as shown below.
This projection ensures that the FP-tree built to represent the clean data, is as small as possible.
Also, if the dataset is read from a `csv` file, like in the code below, it is necessary to [convert](https://gitlab.com/ddcm/ledc-core/-/blob/master/docs/data-model.md#operators) the allergen attributes to integers by using the 'asInteger' function as in the code below.

```java
public static void main(String[] args) throws DataReadException, FileNotFoundException, RepairException, IOException, DataWriteException
{
    //Create a binder to the file with the data
    CSVBinder csvBinder = new CSVBinder(
        new CSVProperties(true, ",", '"'),
        new File("allergen.csv"));

    //Read dateset from csv, but make sure allergen info is read as integers
    Dataset data = new CSVDataReader(csvBinder).readDataWithTypeInference(100);
        
    //Assemble attributes to repair
    Set<String> allergenAttributes = Stream.of(
        "nuts",
        "almondnuts",
        "brazil_nuts",
        "macadamia_nuts",
        "hazelnut",
        "pistachio",
        "walnut",
        "cashew",
        "celery",
        "crustaceans",
        "eggs",
        "fish",
        "gluten",
        "lupin",
        "milk",
        "molluscs",
        "mustard",
        "peanut",
        "sesame",
        "soy",
        "sulfite"
    ).collect(Collectors.toSet());
            
    //Read the rules
    SigmaRuleset rules = ConstraintIo.readSigmaRuleSet(new File("allergens.rules"));
            
    System.out.println("Generate sufficient set...");
    SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset
        .create(rules, new FCFGenerator());
            
    PartialKey partialkey = new PartialKey
    (
        Stream.of("code").collect(Collectors.toSet()),
        allergenAttributes
    );
            
    //Mapping for cost functions
    Map<String, CostFunction<?>> costFunctions = new HashMap<>();

    //A cost map that reflects aversion to risk
    Map<Integer, Map<Integer, Integer>> costMap = new HashMap<>();

    costMap.put(0, new HashMap<>());
    costMap.get(0).put(1,1);
    costMap.get(0).put(2,1);

    costMap.put(1, new HashMap<>());
    costMap.get(1).put(0,3);
    costMap.get(1).put(2,1);

    costMap.put(2, new HashMap<>());
    costMap.get(2).put(0,3);
    costMap.get(2).put(1,3);
            
    for(String a: allergenAttributes)
    {
        costFunctions.put(a, new SimpleIterableCostFunction<>(costMap));
    }
            
    ParkerModel costModel = new ParkerModel(
        partialkey,
        costFunctions,
        new ErrorBasedObjectCost(rules, 4),
        sufficientRules
    );

    //Prepare clean data for frequence repair selection
    Dataset cleanData = data
    .select(o -> sufficientRules.isSatisfied(o))
    .inverseProject("code");

    //Construct repair selection
    RepairSelection selector = new FrequencyRepairSelection(cleanData, sufficientRules);

    //Build the repair engine
    ParkerRepair repairEngine = new ParkerRepair(
        costModel,
        selector);

    //Repair
    Dataset repaired = repairEngine.repair(data);

    //Write the repair to a file
    new CSVDataWriter(
        new CSVBinder(
            new CSVProperties(true, ",", null),
            new File("allergen_repair.csv"))
    ).writeData(repaired);
}
```

### Flight

For the Flight dataset, Parker works best in the scenario where **constant** cost functions are combined with a multiplier that accounts for the **reliability** of a row.
The best results are obtained if the amplifier parameter is set to `4`.
Alternatively, you can use error-aware cost functions that account for `DatetimeErrors.ONE_DAY_OFF`.
These cost functions were not considered during our experiments, but also provide good results.
For the selection of the final repair, it is recommended to use the **RandomRepairSelection** as frequency-based selection does not make sense in this setting.

We conducted our experiments with a version of the flight dataset where timestamps are mapped to integer numbers by computing the *offset* of each timestamp from `2011-11-30` in minutes.
This dataset is found [here](../datasets/flight.csv)
The code below makes this assumption as well, so if you run it, make sure you use the designated dataset.

Because the rules in this setting have no [domain rules](../../docs/sigma-rules.md#sigma-rules) it is necessary to apply a bounding strategy.
Below we use a EnumeratedOffsetBounding with offset 1440 minutes (i.e., one day).
A RangedBounding with the same offset will produce the same results, but the search space for repairs will be larger and repairing will thus be slower.

```java
public static void main(String[] args) throws RepairException, FileNotFoundException, DataReadException, IOException, DataWriteException
{
    CSVBinder csvBinder = new CSVBinder(
        new CSVProperties(true, ",", '\"'),
        new File("flight.csv")
    ); 
        
    //Read dataset and convert attributes to integers
    Dataset data = new CSVDataReader(csvBinder)
        .readData()
        .getAsSimpleDataset()
        .asInteger("scheduled_departure")
        .asInteger("scheduled_arrival")
        .asInteger("actual_departure")
        .asInteger("actual_arrival");
        
        
    System.out.println("Reading rules...");
    SigmaRuleset rules = ConstraintIo
        .readSigmaRuleSet(new File("flight.rules"));

    System.out.println("Generate sufficient set...");
    SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset
        .create(rules, new FCFGenerator());
  
    PartialKey partialkey = new PartialKey
    (
        Stream.of("date_collected", "flight_number").collect(Collectors.toSet()),
        Stream.of(
            "scheduled_departure",
            "actual_departure",
            "scheduled_arrival",
            "actual_arrival"
        ).collect(Collectors.toSet())
    );

    Map<String, CostFunction<?>> costFunctions = new HashMap<>();
        
    costFunctions.put("scheduled_departure", new SimpleErrorFunction<>(DatetimeErrors.ONE_DAY_OFF));
    costFunctions.put("scheduled_arrival", new SimpleErrorFunction<>(DatetimeErrors.ONE_DAY_OFF));
    costFunctions.put("actual_departure", new SimpleErrorFunction<>(DatetimeErrors.ONE_DAY_OFF));
    costFunctions.put("actual_arrival", new SimpleErrorFunction<>(DatetimeErrors.ONE_DAY_OFF));
            
    ParkerModel costModel = new ParkerModel(
        partialkey,
        costFunctions,
        new ErrorBasedObjectCost(rules, 4),
        sufficientRules,
        new EnumeratedOffsetBounding<>(1440)
    );

    //Construct repair selection
    RepairSelection selector = new RandomRepairSelection();

    //Compose repair engine
    ParkerRepair repairEngine = new ParkerRepair(
        costModel,
        selector);

    //Repair
    Dataset repaired = repairEngine.repair(data);

    //Write the repair to a file
    new CSVDataWriter(
        new CSVBinder(
            new CSVProperties(true, ",", null),
            new File("flight_repair.csv"))
    ).writeData(repaired);
}
```
