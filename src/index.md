## Computing Repairs
In this folder, we provide the artifacts to re-compute the repairs of the datasets using the studied approaches: HoloClean, Raha+Baran, and Parker. 
HoloClean and Raha+Baran were excuted with **Python 3.6**. Parker requires **Java 8**. 

In the following, we provide instructions to set up each of the tools and re-run the experiments using the same datasets and configurations used in the paper. 

---

### HoloClean 
To repeat the experiments with HoloClean, we provide in this folder the Python script `run_holoclean.py`. 
Since HoloClean is not available as a Python package, this script requires some simple configurations before running it which are explained in the following instructions.  

#### Instructions
1. Download the source code of HoloClean from the official [Git Hub repository](https://github.com/HoloClean/holoclean). Follow the instructions provided there to set up the database on further requirements to run HoloClean. 
1. Configure the script `run_holoclean.py` on _line 3_ to provide the full path to the source code of HoloClean. 
1. In the `run_holoclean.py`, set the variable `data_path` on _line 10_ to provide the full path to the folder `datasets` provided in this repository.
1. Run the Python script as follows: ``python run_holoclean.py``. 
  
#### Exporting HoloClean Repairs
The repaired datasets are stored as tables in the database. 

In these experiments, 8 tables have been created with the repairs of the three studied datasets using different HoloClean configurations.
The names of the tables are: 
`eudract_repaired`, `eudract_violation_repaired`, `eudract_null_repaired`, `flight_repaired`, `flight_violation_repaired`, `flight_null_repaired`, `allergen_repaired`, and `allergen_violation_repaired`.

To access the tables, first we log in:  

```` 
psql -U holocleanuser -W holo
Password: abcd1234
```` 

To export each of them as `.csv` files, we can execute the following command:
```` 
\COPY (SELECT * FROM eudract_repaired ORDER BY _tid_) TO 'holoclean_eudract.csv' DELIMITER ',' CSV HEADER;
````
Repeat the previous command for each table. 

---

### Raha+Baran 
To repeat the experiments with Raha and Baran, we provide in this folder the Python script `run_raha_baran.py`. 
This script can directly be run as it is using the data provided in this repository. Alternatively, it can also be configured as follows in the `__main__` function:
- `runs`: variable that indicates the number of executions per dataset. This is currently set to `10`. 
- `outdir`: path of the output folder where Raha and Baran will write the repaired datasets and the sampled tuples. 
  By default, this is set to the current directory (`./`).  

#### Instructions
1. Install Raha and Baran from the official [Git Hub repository](https://github.com/BigDaMa/raha). Follow the instructions to install the tool as a Python package using `pip3`. 
1. Run the Python script provided in this folder to reproduce the experiments as follows: 
``python run_raha_baran.py``.
1. The repaired datasets are automatically exported as `.csv` files in the directory specified in `outdir`. 
   
--- 
### Parker

#### Instructions
1. Install the latest version of [Java](https://java.com/en/download/help/download_options.html). You require at least Java 8.
2. Install [Maven](https://maven.apache.org/install.html).
3. In your Java IDE, make a new Java/Maven project to run the experiments in.
4. Open the `pom.xml` file of this new project and add the following snippet:
```xml
<dependencies>
  <dependency>
    <groupId>be.ugent.ledc</groupId>
    <artifactId>sigma</artifactId>
    <version>1.0-SNAPSHOT</version>
  </dependency>
</dependencies>
```
5. Declare the repository in the `pom.xml` file by adding:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/12364642/-/packages/maven</url>
  </repository>
</repositories>
```
6. For each of the datasets we used in our experiments, you can find source code [here](parker.md). Make sure the datasets and constraint files are located in the correct directory. The repairs will be written to a `.csv` file.
