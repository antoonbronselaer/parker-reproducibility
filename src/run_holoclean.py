import sys
# Provide path to the holoclean source code, the last part of the path should be /holoclean
sys.path.append('')
import holoclean
from detect import NullDetector, ViolationDetector
from repair.featurize import *
import time

# Provide full path to datasets
data_path = ""

def run_holoclean(table_name, input_data, constraints, list_detectors):

    # 1. Setup a HoloClean session
    t1 = time.time()
    hc = holoclean.HoloClean(
        db_name='holo',
        domain_thresh_1=0,
        domain_thresh_2=0,
        weak_label_thresh=0.99,
        max_domain=10000,
        cor_strength=0.6,
        nb_cor_strength=0.8,
        epochs=10,
        weight_decay=0.01,
        learning_rate=0.001,
        threads=1,
        batch_size=1,
        verbose=True,
        timeout=3*60000,
        feature_norm=False,
        weight_norm=False,
        print_fw=True
    ).session

    # 2. Load training data and denial constraints
    t2 = time.time()
    hc.load_data(table_name, input_data)
    hc.load_dcs(constraints)
    hc.ds.set_constraints(hc.get_dcs())

    # 3. Detect erroneous cells using these two detectors
    t3 = time.time()
    detectors = list_detectors
    hc.detect_errors(detectors)

    # 4. Repair errors utilizing the defined features
    t4 = time.time()
    hc.setup_domain()
    featurizers = [
        InitAttrFeaturizer(),
        OccurAttrFeaturizer(),
        FreqFeaturizer(),
        ConstraintFeaturizer(),
    ]
    hc.repair_errors(featurizers)
    t5 = time.time()

    print("Setup time", t2 - t1)
    print("Load data time", t3 - t2)
    print("Detect error times", t4 - t3)
    print("Repair errors time", t5 - t4)


def exp1():
    table_name = 'eudract'
    input_data = data_path+'eudract.csv'
    constraints = data_path+'eudract_constraints_holoclean.txt'
    list_detectors = [NullDetector(), ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


def exp2():
    table_name = 'eudract_violation'
    input_data = data_path+'eudract.csv'
    constraints = data_path+'eudract_constraints_holoclean.txt'
    list_detectors = [ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


def exp3():
    table_name = 'eudract_null'
    input_data = data_path+'eudract.csv'
    constraints = data_path+'eudract_constraints_holoclean.txt'
    list_detectors = [NullDetector()]
    return table_name, input_data, constraints, list_detectors

def exp4():
    table_name = 'flight'
    input_data = data_path+'flight.csv'
    constraints = data_path+'flight_constraints_holoclean.txt'
    list_detectors = [NullDetector(), ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


def exp5():
    table_name = 'flight_violation'
    input_data = data_path+'flight.csv'
    constraints = data_path+'flight_constraints_holoclean.txt'
    list_detectors = [ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


def exp6():
    table_name = 'flight_null'
    input_data = data_path+'flight.csv'
    constraints = data_path+'flight_constraints_holoclean.txt'
    list_detectors = [NullDetector()]
    return table_name, input_data, constraints, list_detectors


def exp7():
    table_name = 'allergen'
    input_data = data_path+'allergen.csv'
    constraints = data_path+'allergen_constraints_holoclean.txt'
    list_detectors = [NullDetector(), ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


def exp8():
    table_name = 'allergen_violation'
    input_data = data_path+'allergen.csv'
    constraints = data_path+'allergen_constraints_holoclean.txt'
    list_detectors = [ViolationDetector()]
    return table_name, input_data, constraints, list_detectors


if __name__ == '__main__':

    # Run all experiments
    for exp in range(1,9):
        # Get experiment configuration.
        vals = locals()['exp'+str(exp)]()

        # Run experiment
        print("running ", vals)
        run_holoclean(*vals)

