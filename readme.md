## Reproducibility page: "Parker: Data fusion through consistent repairs using edit rules under partial keys" [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/antoonbronselaer%2Fparker-reproducibility/HEAD)

This repository contains the artifacts to reproduce the results presented in the paper _**"Parker: Data fusion through consistent repairs using edit rules under partial keys"**_ by Bronselaer and Acosta.

The experimental study presented in this paper includes the evaluation of the approaches _Parker_, [_Holistic_](https://github.com/daqcri/NADEEF/tree/master), [_HoloClean_](https://github.com/HoloClean/holoclean), and [_Raha+Baran_](https://github.com/BigDaMa/raha). 

The content of each sub-folder is desribed in the following.  

### analysis
Includes the Jupyter notebook and Python scripts to reproduce the figures and tables reported in the experimental study of the paper. 

### datasets
Contains the datasets _EudraCT_, _Allergen_, _Flight_ and their corresponding gold standards. 

In addition, the subsets used for executing Raha+Baran are also included (and denoted with `baran` in the file names). 

### repairs
Contains the repaired datasets (exported as `csv` files) computed with the studied approaches using different configurations.   

### src
Contains the scripts and further information to execute the approaches. More details are available in the [index.md](src/index.md) file in that folder.   
